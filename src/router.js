import Vue from 'vue'
import Router from 'vue-router'

import HomeIndex from '@/views/home/Index';
import ProductDetail from '@/views/products/Detail';

Vue.use(Router);

export default new Router({
  mode: 'history',
  scrollBehavior() {
    return {x: 0, y: 0}
  },
  routes: [

    {
      path: '/',
      name: 'home.index',
      components: {
        default: HomeIndex,
      }
    },

    {
      props: true,
      path: '/products/:slug',
      name: 'products.detail',
      component: ProductDetail
    },

    {
      props: true,
      path: '/products',
      name: 'products.collection',
      component: () => import('@/views/products/Collection.vue')
    },

    {
      props: true,
      path: '/collections',
      name: 'collections.collection',
      component: () => import('@/views/collections/Collection.vue')
    },

    {
      props: true,
      path: '/collections/:slug',
      name: 'collections.detail',
      component: () => import('@/views/collections/Detail.vue')
    },

    {
      props: true,
      path: '/cart',
      name: 'cart.index',
      component: () => import('@/views/cart/Index.vue')
    },

    {
      props: true,
      path: '/checkout',
      name: 'checkout.index',
      component: () => import('@/views/checkout/Index.vue')
    },

    {
      path: '/payment/successful',
      name: 'payment.successful',
      component: () => import('@/views/payment/Successful.vue')
    },

    {
      path: '/payment/cancelled',
      name: 'payment.cancelled',
      component: () => import('@/views/payment/Cancelled.vue')
    },

    {
      path: '/pages',
      name: 'pages.collection',
      component: () => import('@/views/pages/Collection.vue')
    },

    {
      props: true,
      path: '/pages/:slug',
      name: 'pages.detail',
      component: () => import('@/views/pages/Detail.vue')
    }


  ]
});
