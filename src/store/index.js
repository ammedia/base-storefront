import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import cache from './modules/cache';

import JsonApi from '@/helpers/JsonApi';

export default new Vuex.Store({

    modules: { cache },

    state: {
      auth: null,

      cart: null,
      cartJsonApi: null,

      loadingCart: false,

    },

    mutations: {

      auth(state, auth) {
        state.auth = auth;
      },

      cart(state, cart) {
        state.cart = cart;
        state.cartJsonApi = new JsonApi(cart);
      },

      loadingCart(state, status) {
        state.loadingCart = status;
      }

    }

})
