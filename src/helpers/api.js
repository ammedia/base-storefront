import $ from 'jquery';

import faker from './faker';

import store from '@/store';

class Api {

  constructor() {
    
    this.env = process.env.NODE_ENV;

    // Api 
    this.apiUrl = (this.env === 'production') ? window.apiUrl : '';
    this.shopId = (this.env === 'production') ? window.shop.id : '';

  }

  get(path, params, loadNew) {

    if (this.env === 'development') {
      return faker.data('get', path, params);
    }

    if (typeof params !== 'object') {
      params = {};
    }

    params.shop_id = this.shopId;

    return new Promise((resolve, reject) => {

      // Cache
      let cacheKey = path + JSON.stringify(params);
      if (!loadNew && typeof store.state.cache[cacheKey] !== 'undefined') {
        return resolve(JSON.parse(store.state.cache[cacheKey]));
      }

      $.ajax({
        url: this.apiUrl + path,
        data: params,
        dataType: 'json',
        error: error => {
          return reject(error);
        },
        success: result => {

          let data = {
            data: result
          };

          // Save to cache
          store.commit('cache/put', {
            key: cacheKey,
            value: JSON.stringify(data)
          });

          return resolve(data);
        }
      });

    });

  }

  post(path, params) {

    if (this.env === 'development') {
      return faker.data('post', path, params);
    }

    if (typeof params !== 'object') {
      params = {};
    }

    return new Promise((resolve, reject) => {

      $.ajax({
        url: this.apiUrl + path,
        type: 'post',
        dataType: 'json',
        headers: {
          'shop-id': window.shop.id
        },
        data: JSON.stringify(params),

        error: error => {
          return reject(error);
        },

        success: result => {
          return resolve({
            data: result
          });
        }

      });

    });
  }

  patch(path, params) {

    if (this.env === 'development') {
      return faker.data('patch', path, params);
    }

    if (typeof params !== 'object') {
      params = {};
    }

    return new Promise((resolve, reject) => {

      $.ajax({
        url: this.apiUrl + path,
        type: 'patch',
        dataType: 'json',
        headers: {
          'shop-id': window.shop.id
        },
        data: JSON.stringify(params),
        error: error => {
          return reject(error);
        },
        success: result => {
          return resolve({
            data: result
          });
        }
      });

    });

  }

  delete(path, params) {

    if (this.env === 'development') {
      return faker.data('delete', path, params);
    }

    if (typeof params !== 'object') {
      params = {};
    }

    return new Promise((resolve, reject) => {

      $.ajax({
        url: this.apiUrl + path,
        type: 'delete',
        dataType: 'json',
        headers: {
          'shop-id': window.shop.id
        },
        data: JSON.stringify(params),
        error: error => {
          return reject(error);
        },
        success: result => {
          return resolve({
            data: result
          });
        }
      });

    });

  }

}

export default new Api();