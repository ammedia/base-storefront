import api from '@/helpers/api';

import JsonApi from '@/helpers/JsonApi';
import seoHelper from '@/helpers/seo';

export default {

  props: ['slug'],

  data() {
    return {
      collectionJsonApi: null,
      productsJsonApi: null,
      productLimit: 20,
      error: null
    }
  },

  created() {

    // Load collection
    this.loadCollection();
  },

  methods: {

    loadCollection() {

      this.collection = null;

      api.get('collections', {
        slug: this.slug
      }).then(response => {

        if (typeof response.data.data !== 'undefined') {
          this.collectionJsonApi = new JsonApi(response.data);
        }

        // SEO
        seoHelper.setTitle(this.collectionJsonApi.document.data[0].attributes.title);
        seoHelper.setDescription(this.collectionJsonApi.document.data[0].attributes.description);

        // Load products
        this.loadProducts();

      }).catch(error => {

        this.error = typeof error.response !== 'undefined' ? error.response.data.errors[0].title : error.message;

      });

    },

    loadProducts() {

      this.productsJsonApi = null;

      api.get('products', {
        collection_id: this.collectionJsonApi.document.data[0].id,
        limit: this.productLimit,
        page: this.page
      }).then(response => {

        if (typeof response.data.data !== 'undefined' && response.data.data.length) {
          this.productsJsonApi = new JsonApi(response.data);
        } else {
          this.productsJsonApi = [];
        }

      }).catch(error => {
        this.productsJsonApi = [];
      });
    }

  },

  watch: {

    slug: function() {
      this.loadCollection();
    }

  },

  computed: {

    page: function() {

      if (typeof this.$route.query.page !== 'undefined') {
        return 1;
      }

      return this.$route.query.page;

    }

  }

}