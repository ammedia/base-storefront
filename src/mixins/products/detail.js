import JsonApi from '@/helpers/JsonApi';
import currencyHelper from '@/helpers/currency';
import cartHelper from '@/helpers/cart';
import seoHelper from '@/helpers/seo';

import Error from '@/views/Error';

import Loader from '@/components/Loader';

import api from '@/helpers/api';

export default {

  props: ['slug'],

  components: { Loader, Error },

  data() {

    return {
      currencyHelper,
      
      productJsonApi: null,

      activeVariantId: null,
      activeVariant: null,
      
      qty: 1,

      addingToCart: false,

      error: null,

    };

  },

  created() {

    this.loadData();

    // Option value change
    this.$on('change-option-value', data => {

      let variants = this.productJsonApi.findRelationshipResources(this.productJsonApi.document.data, 'variants');

      let newOptionValueData;
      if (this.activeVariant) {

        newOptionValueData = JSON.parse(JSON.stringify(this.activeVariant.relationships.option_values.data));

        for (let i = 0; i < newOptionValueData.length; i++) {

          if (newOptionValueData[i].id === data.currentId) {
            newOptionValueData[i].id = data.activeId;
            break;
          }

        }

      }
        
      // Compare data maker
      let compareDataMaker = function(optionValueData) {
        
        let compareData = [];

        for (let i = 0; i < optionValueData.length; i++) {
          compareData.push(optionValueData[i].id);
        }

        compareData.sort();

        return JSON.stringify(compareData);

      }

      // If active variant is set
      if (this.activeVariant) {
      
        // Find new active variant
        for (let i = 0; i < variants.length; i++) {

          let compareOptionValueData = JSON.parse(JSON.stringify(variants[i].relationships.option_values.data));
          compareOptionValueData.sort();

          if (compareDataMaker(newOptionValueData) === compareDataMaker(compareOptionValueData)) {

            this.activeVariantId = variants[i].id;

            // If found but unavailable
            if (this.unavailableVariantIds.indexOf(this.activeVariantId) > -1) {
              this.activeVariantId = null;
              this.activeVariant = null;
            } else {

              // Set active variant
              this.setActiveVariant();
              return;

            }

          }

        }

      }

      // If active variant is not set
      if (!this.activeVariant) { 

        for (let i = 0; i < variants.length; i++) {

          // Find a variant available with selected option
          for (let k = 0; k < variants[i].relationships.option_values.data.length; k++) {

            // Found and available
            if (variants[i].relationships.option_values.data[k].id === data.activeId && this.unavailableVariantIds.indexOf(variants[i].id) === -1) {
              this.activeVariantId = variants[i].id;
              this.setActiveVariant();
              return;
            }

          }

        }

      }

    });

  },

  methods: {

    async loadData() {
      
      this.productJsonApi = null;

      // data
      let find = await api.get('products/' + this.slug).catch(error => {
        this.error = error;
      });

      if (this.error) {
        return;
      }

      // SEO helper
      seoHelper.setTitle(find.data.data.attributes.title);
      seoHelper.setDescription(find.data.data.attributes.description);

      // Json Api data
      this.productJsonApi = new JsonApi(find.data);

      // Set active variant
      this.setActiveVariant();

    },

    setActiveVariant: function() {

      // If all variants are not available
      if (this.unavailableVariants.length === this.productJsonApi.document.data.relationships.variants.data.length) {
        return false;
      }

      // First variant will be activated by default
      if (!this.activeVariantId) {
          
        for (let i = 0; i < this.productJsonApi.document.data.relationships.variants.data.length; i++) {

          if (this.unavailableVariantIds.indexOf(this.productJsonApi.document.data.relationships.variants.data[i].id) === -1) {
            this.activeVariantId = this.productJsonApi.document.data.relationships.variants.data[i].id;
            break;
          }

        }

      }

      let variants = this.productJsonApi.findRelationshipResources(this.productJsonApi.document.data, 'variants', { id: this.activeVariantId });

      for (let i = 0; i < variants.length; i++) {

        if (variants[i].id === this.activeVariantId) {
          return this.activeVariant = variants[i];
        }

      }
    },

    changeQty(number) {
      this.qty += number;
    },

    async addToCart() {

      // Check variant available
      if (this.unavailableVariantIds.indexOf(this.activeVariantId) > -1) {
        return alert('The option you selected (' + this.activeVariant.attributes.title + ') is not available.');
      }

      if (!this.activeVariantId) {
        return alert('Please select your option first');
      }

      this.addingToCart = true;

      let error = null;

      let cartItem = await cartHelper.add(this.activeVariantId, this.qty).catch(error => {
        error = true;
      });

      this.addingToCart = false;

      if (!error) {
        // Redirect to cart page
        this.$router.push({ name: 'cart.index' });
      }

    }

  },

  watch: {

    qty: function() {
      if (this.qty < 1) {
        this.qty = 1;
      }
    },

    slug: function() {
      this.loadData();
    }

  },

  computed: {

    unavailableVariantIds() {

      let ids = [];

      for (let i = 0; i < this.unavailableVariants.length; i++) {
        ids.push(this.unavailableVariants[i].id);
      }

      return ids;

    },

    unavailableVariants() {

      let unavailableVariants = [];
      let variants = this.productJsonApi.findRelationshipResources(this.productJsonApi.document.data, 'variants');

      for (let i = 0; i < variants.length; i++) {

        if (variants[i].attributes.status !== 'available') {
          unavailableVariants.push(variants[i]);
        }

      }

      return unavailableVariants;

    },

    images: function() {

      if (this.activeVariant) {
        let variantImages = this.productJsonApi.findRelationshipResources(this.activeVariant, 'images');

        if (variantImages && variantImages.length) {
          return variantImages;
        }
      }

      return this.productJsonApi.findRelationshipResources(this.productJsonApi.document.data, 'images');

    },

    tagIds: function() {

      let tagIds = [];
      let tags = this.productJsonApi.findRelationshipResources(this.productJsonApi.document.data, 'tags');

      if (!tags) {
        return [];
      }

      for (let i = 0; i < tags.length; i++) {
        tagIds.push(tags[i].id);
      }

      return tagIds;

    }

  }

}